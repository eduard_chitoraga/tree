<?php
class Create{
    const host="localhost";
    const db = "tree_task";
    const root="root";
    const root_password="";

    private $conn = null;
    public function __construct(){
        $this->load();
    }
    public function db()
    {
        return Create::db;
    }
    public function load()
    {

        try {
            $sql1 = "
                CREATE TABLE `district_structure` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `lft` int(10) unsigned NOT NULL,
                  `rgt` int(10) unsigned NOT NULL,
                  `lvl` int(10) unsigned NOT NULL,
                  `pid` int(10) unsigned NOT NULL,
                  `pos` int(10) unsigned NOT NULL,
                  PRIMARY KEY (`id`)
                );

                INSERT INTO `district_structure` VALUES ('1', '0', '5', '0', '0', '0');
                INSERT INTO `district_structure` VALUES ('5', '1', '2', '1', '1', '0');
                INSERT INTO `district_structure` VALUES ('6', '3', '4', '1', '1', '1');



                CREATE TABLE `districts` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
                  `description` varchar(255) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                );
                INSERT INTO `districts` VALUES ('1', 'root', 'root');
                INSERT INTO `districts` VALUES ('5', 'Chisinau', 'Capitala RM');
                INSERT INTO `districts` VALUES ('6', 'Soroca', 'Cetatea Sorocii');

            ";
            $dbh = new PDO("mysql:host=".Create::host, Create::root, Create::root_password);
            $r = $dbh->exec("CREATE DATABASE IF NOT EXISTS ".Create::db);
            if($r)
            {
                $dbh = new PDO("mysql:host=".Create::host.";dbname=".Create::db, Create::root, Create::root_password);
                $dbh->exec($sql1);
            }

        } catch (PDOException $pe) {
            return true;
        }
    }
}