/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : tree_task

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-01-15 15:35:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `districts`
-- ----------------------------
DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of districts
-- ----------------------------
INSERT INTO `districts` VALUES ('1', 'root', 'root');
INSERT INTO `districts` VALUES ('5', 'Chisinau', 'Capitala RM');
INSERT INTO `districts` VALUES ('6', 'Soroca', 'Cetatea Sorocii');

-- ----------------------------
-- Table structure for `district_structure`
-- ----------------------------
DROP TABLE IF EXISTS `district_structure`;
CREATE TABLE `district_structure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lft` int(10) unsigned NOT NULL,
  `rgt` int(10) unsigned NOT NULL,
  `lvl` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `pos` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of district_structure
-- ----------------------------
INSERT INTO `district_structure` VALUES ('1', '0', '5', '0', '0', '0');
INSERT INTO `district_structure` VALUES ('5', '1', '2', '1', '1', '0');
INSERT INTO `district_structure` VALUES ('6', '3', '4', '1', '1', '1');
